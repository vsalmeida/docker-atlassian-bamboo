![Atlassian Bamboo](https://www.atlassian.com/dam/wac/legacy/bamboo_logo_landing.png)

Bamboo is a continuous integration and deployment tool that ties automated builds, tests and releases together in a single workflow.

Learn more about Bamboo: <https://www.atlassian.com/software/bamboo>

# Overview

This Docker container makes it easy to get an instance of Bamboo up and running.

# Quick Start

For the `BAMBOO_HOME` directory that is used to store the repository data (amongst other things) we recommend mounting a host directory as a [data volume](https://docs.docker.com/engine/tutorials/dockervolumes/#/data-volumes), or via a named volume if using a docker version >= 1.9. 

To get started you can use a data volume, or named volumes. In this example we'll use named volumes.

    $> docker volume create --name bambooVolume
    $> docker run -v bambooVolume:/var/atlassian/application-data/bamboo --name="bamboo" -d -p 8085:8085 -p 54663:54663 dchevell/bamboo


**Success**. Bamboo is now available on [http://localhost:8085](http://localhost:8085)*

Please ensure your container has the necessary resources allocated to it. We recommend 2GiB of memory allocated to accommodate the application server. See [System Requirements](https://confluence.atlassian.com/display/BAMBOO/Bamboo+Best+Practice+-+System+Requirements) for further information.
    

_* Note: If you are using `docker-machine` on Mac OS X, please use `open http://$(docker-machine ip default):8085` instead._

## Memory / Heap Size

If you need to override Bamboo's default memory allocation, you can control the minimum heap (Xms) and maximum heap (Xmx) via the below environment variables.

* `JVM_MINIMUM_MEMORY` (default: 512m)

   The minimum heap size of the JVM

* `JVM_MAXIMUM_MEMORY` (default: 1024m)

   The maximum heap size of the JVM

## Reverse Proxy Settings

If Bamboo is run behind a reverse proxy server as [described here](https://confluence.atlassian.com/kb/integrating-apache-http-server-reverse-proxy-with-bamboo-753894403.html), then you need to specify extra options to make Bamboo aware of the setup. They can be controlled via the below environment variables.

* `CATALINA_CONNECTOR_PROXYNAME` (default: NONE)

   The reverse proxy's fully qualified hostname.

* `CATALINA_CONNECTOR_PROXYPORT` (default: NONE)

   The reverse proxy's port number via which Bamboo is accessed.

* `CATALINA_CONNECTOR_SCHEME` (default: http)

   The protocol via which Bamboo is accessed.

* `CATALINA_CONNECTOR_SECURE` (default: false)

   Set 'true' if CATALINA_CONNECTOR_SCHEME is 'https'.
   
* `CATALINA_CONTEXT_PATH` (default: NONE)

   The context path the application is served over.

## JVM configuration

If you need to pass additional JVM arguments to Bamboo, such as specifying a custom trust store, you can add them via the below environment variable

* `JVM_SUPPORT_RECOMMENDED_ARGS`

   Additional JVM arguments for Bamboo
   
Example:

    $> docker run -e JVM_SUPPORT_RECOMMENDED_ARGS=-Djavax.net.ssl.trustStore=/var/atlassian/application-data/bamboo/cacerts -v bambooVolume:/var/atlassian/application-data/bamboo --name="bamboo" -d -p 8085:8085 -p 54663:54663 dchevell/bamboo

# Upgrade

To upgrade to a more recent version of Bamboo you can simply stop the `bamboo` container and start a new one based on a more recent image:

    $> docker stop bamboo
    $> docker rm bamboo
    $> docker run ... (See above)

As your data is stored in the data volume directory on the host it will still  be available after the upgrade.

_Note: Please make sure that you **don't** accidentally remove the `bamboo` container and its volumes using the `-v` option._

# Backup

For evaluations you can use the built-in database that will store its files in the Bamboo home directory. In that case it is sufficient to create a backup archive of the docker volume.

If you're using an external database, you can configure Bamboo to make a backup automatically each night. This will back up the current state, including the database to the `bambooVolume` docker volume, which can then be archived. Alternatively you can backup the database separately, and continue to create a backup archive of the docker volume to back up the Bamboo Home directory.

Read more about data recovery and backups: [https://confluence.atlassian.com/display/BAMBOO/Data+and+backups](https://confluence.atlassian.com/display/BAMBOO/Data+and+backups)

# Versioning

The `latest` tag matches the most recent release of Atlassian Bamboo. Thus `dchevell/bamboo:latest` will use the newest version of Bamboo available.

Alternatively you can use a specific major, major.minor, or major.minor.patch version of Bamboo by using a version number tag:

* `dchevell/bamboo:6`
* `dchevell/bamboo:6.6`
* `dchevell/bamboo:6.6.3`

All versions from 6.0+ are available

# Support

This Docker container is unsupported and is intended for illustration purposes only.
