#!/bin/sh
set -euo pipefail

echo "Waiting for app to start at ${URL}"
i=0
until error="$(wget --spider "$URL" 2>&1)"; do
    if [ ${i} -gt 60 ]; then
        echo
        echo '!!! Timed out waiting for app to start up!'
        printf '%s\n' "$error" >&2
        exit 1
    fi
    printf '.'
    i=$((i+1))
    sleep 5
done

echo

# Now the container is up and running, test a page is returning expected content
echo "Checking ${URL} for expected return value"
if ! wget -qS -O - "${URL}" | grep "${TEST_STRING}"; then
    echo "Unexpected response"
    exit 1
else
    echo "App seems to be up and running"
    exit 0
fi
